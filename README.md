```bash
## 构建镜像
docker build -t cra .

## 启动镜像
docker run \
  -d \
  --name cra \
  --rm \
  -u root \
  -p 3000:80 \
  cra

## jenkinsci
docker run \
  -u root \
  --rm \
  -d \
  -p 8080:8080 \
  -p 50000:50000 \
  -v jenkins-data:/var/jenkins_home \
  -v /var/run/docker.sock:/var/run/docker.sock \
  jenkinsci/blueocean
```